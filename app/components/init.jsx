import React, { Component}  from 'react'
import {Grid, Row} from 'react-flexbox-grid/lib'
import Box from './box'
import style from './style'

class Init extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Grid fluid className={style.grid}>
        <Row>

          <Box type="row" xs={12} sm={12} md={12} lg={12}>
            <Row>
              <Box type="row" xs={6} sm={6} md={6} lg={6}>
		hola
              </Box>
              <Box type="row" xs={6} sm={6} md={6} lg={6}>
		mundo
              </Box>
            </Row>
          </Box>

        </Row>
      </Grid>
    )
  }
}

export default Init
