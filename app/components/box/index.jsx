import React, {PropTypes} from 'react';
import {Col} from 'react-flexbox-grid/lib';
import style from './style';

const Box = (props) => {
  return (
    <Col {...props} className={style.Col}>
        <div className = {style[props.type || 'box']}>
          {props.children}
        </div>
    </Col>
  );
};

Box.propTypes = {
  type: PropTypes.oneOf(['row', 'container', 'nested', 'large']).isRequired,
  children: PropTypes.node
};

export default Box;
