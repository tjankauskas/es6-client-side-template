import 'babel-polyfill'
import React from 'react'
import ReactDOM from 'react-dom'
import Init from './components/init'

ReactDOM.render(<Init />, document.getElementById('app'))
