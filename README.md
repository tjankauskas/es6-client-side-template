# Basic client side template + the best 

* ####  ES6 + ES7 features (async, await)
* ####  ESLint.
* ####  Scss for styling

### Steps to build the project:

  - `npm start` - starts 'hot reloaded' project on localhost:8080
  - `webpack` - generates app.js and app.css